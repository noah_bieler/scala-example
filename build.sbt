lazy val root = (project in file("."))
  .settings(
    organization := "org.example", 
    name := "base",
    version := "0.1.0",
    scalaVersion := "2.11.6",
    mainClass in (Compile, run) := Some("org.example.App")
  )
